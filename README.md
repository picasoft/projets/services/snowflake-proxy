# Snowflake Proxy

Le [proxy snowflake](https://snowflake.torproject.org/) est un sous-projet de Tor permettant d'accéder au réseau Tor, y compris dans les pays où les noeuds sont censurés.

L'idée est de faire passer le traffic Tor à travers un flux WebRTC chiffré, faisant passer le contenu pour un flux quelconque. 

La plupart des proxy Snowflakes sont faits pour être éphémères (ils tournent dans un onglet de navigateur, grâce à WebRTC), mais Tor suggère qu'il est intéressant d'avoir des [instances fixes](https://community.torproject.org/relay/setup/snowflake/standalone/) qui offrent une connexion rapide, fiable, plus facilement utilisable derrière des pare-feux restrictifs.

[Q] Je ne sais pas si le fait d'avoir un proxy avec une IP fixe peut, à terme, intégrer notre IP dans le système des censeurs.

Tor recommande de lancer le proxy à côté d'un conteneur [watchtower](https://github.com/containrrr/watchtower), qui automatise la mise à jour de l'image. C'est particulièrement recommandé dans le cadre du jeu chat/souris de la censure.

Watchtower, pour des raisons de sécurité, accède à la socket Docker à travers [docker-socket-proxy](https://github.com/Tecnativa/docker-socket-proxy).